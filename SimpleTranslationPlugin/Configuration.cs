﻿using Dalamud.Configuration;
using Dalamud.Plugin;
using System;

namespace SimpleTranslationPlugin
{
    [Serializable]
    public class Configuration : IPluginConfiguration
    {
        public int Version { get; set; } = 0;

        public string deepl_key { get; set; } = "";
        public string deepl_endpoint { get; set; } = "https://api.deepl.com/v2/translate";
        public string local_endpoint { get; set; } = "http://localhost:9000/message";
        public bool use_local_endpoint { get; set; } = false;
        public bool remove_accents { get; set; } = false;
        public string deepl_language { get; set; } = "it";
        public string gender { get; set; } = "";
        public float window_size { get; set; } = 1f;

        [NonSerialized]
        private DalamudPluginInterface pluginInterface;

        public void Initialize(DalamudPluginInterface pluginInterface)
        {
            this.pluginInterface = pluginInterface;
        }

        public void Save()
        {
            this.pluginInterface.SavePluginConfig(this);
        }
    }
}
