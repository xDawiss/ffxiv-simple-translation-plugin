﻿﻿using System;
using Dalamud.Plugin;
using Dalamud.Data;
using Dalamud.Game.Command;
using Dalamud.Game.Internal;
using Dalamud.Game.Internal.Gui.Addon;
using Dalamud.Game.Text.SeStringHandling;
using FFXIVClientStructs.FFXIV.Client.UI;
using FFXIVClientStructs.FFXIV.Component.GUI;
using System.Runtime.InteropServices;
using System.Net.Http;
using System.Collections.Generic;
using XivCommon;
using FFXIVClientStructs.FFXIV.Client.System.String;
using System.Text;
using System.Globalization;
using Dalamud.Game.ClientState.Structs;
using System.Reflection;
using System.Linq;

namespace SimpleTranslationPlugin
{
    public class Plugin : IDalamudPlugin
    {
        public string Name => "Simple Translation Plugin";

        private const string commandName = "/translation";
        private DalamudPluginInterface pi;
        private XivCommonBase common { get; set; }
        private XivCommonBase commonBT { get; set; }
        private Configuration configuration;
        private PluginUI ui;
        private HttpClient httpClient;
        public string PlayerName => pi?.ClientState?.LocalPlayer?.Name;
        public string PlayerGender = "";

        // When loaded by LivePluginLoader, the executing assembly will be wrong.
        // Supplying this property allows LivePluginLoader to supply the correct location, so that
        // you have full compatibility when loaded normally and through LPL.
        public string AssemblyLocation { get => assemblyLocation; set => assemblyLocation = value; }
        private string assemblyLocation = System.Reflection.Assembly.GetExecutingAssembly().Location;

        public void Initialize(DalamudPluginInterface pluginInterface)
        {
            pi = pluginInterface;
            common = new XivCommonBase(pi, Hooks.Talk);
            commonBT = new XivCommonBase(pi, Hooks.BattleTalk);
            // Configuration
            configuration = pi.GetPluginConfig() as Configuration ?? new Configuration();
            configuration.Initialize(pi);
            // HTTP
            httpClient = new HttpClient();
            // Do not hide during cutscenes
            pi.UiBuilder.DisableAutomaticUiHide = true;
            // Embed the UI
            ui = new PluginUI(configuration);
            // Add the command to the chat
            pi.CommandManager.AddHandler(commandName, new CommandInfo(OnCommand)
            {
                HelpMessage = "Display the translation of NPC dialogues"
            });
            // Add UI to hook for creating it
            pi.UiBuilder.OnBuildUi += DrawUI;
            pi.UiBuilder.OnOpenConfigUi += (sender, args) => DrawConfigUI();
            // Call the handler for dialogue
            common.Functions.Talk.OnTalk += TalkEvent;
            commonBT.Functions.BattleTalk.OnBattleTalk += BattleTalkEvent;
            // Get player gender
            PlayerGender = configuration.gender;
        }

        public void Dispose()
        {
            ui.Dispose();
            common.Functions.Talk.OnTalk -= TalkEvent;
            commonBT.Functions.BattleTalk.OnBattleTalk -= BattleTalkEvent;
            pi.CommandManager.RemoveHandler(commandName);
            common.Dispose();
            pi.Dispose();
        }

        private void OnCommand(string command, string args)
        {
            // Display translation on command
            if (configuration.deepl_endpoint.Length == 0)
            {
                ui.SettingsVisible = true;
                return; 
            }
            if (configuration.deepl_language.Length != 2)
            {
                ui.SettingsVisible = true;
                return; 
            }
            if (configuration.deepl_key.Length == 0)
            {
                ui.SettingsVisible = true;
                return; 
            }
            ui.Visible = true;
        }

        private void DrawUI()
        {
            ui.Draw();
        }

        private void DrawConfigUI()
        {
            ui.SettingsVisible = true;
        }
        private string RemoveAccents(string text)
        {
            byte[] tempBytes;
            tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(text);
            string noAccentsText = System.Text.Encoding.UTF8.GetString(tempBytes);
            return noAccentsText;
        }
        private void applyTranslation(string author, string text)
        {
            ui.changeAuthor(author);
            if(configuration.remove_accents)
            {
                ui.changeText(RemoveAccents(text));
            }
            else
            {
                ui.changeText(text);
            }
        }

        private void BattleTalkEvent(ref SeString sender, ref SeString message, ref XivCommon.Functions.BattleTalkOptions options, ref bool isHandled)
        {
            if (!ui.Visible)
            {
                var backupData = new Dictionary<string, string>(){
                    {"speaker_name", sender.TextValue },
                    {"player_name", pi.ClientState.LocalPlayer.Name},
                    {"player_gender", PlayerGender },
                    {"translation", ""},
                    {"original", message.TextValue}
                };

                saveToLocalhost(backupData);

                return;
            }
            PluginLog.Verbose(message.TextValue);
            translateTalk(sender, message);
        }

        private void TalkEvent(ref SeString name, ref SeString text, ref XivCommon.Functions.TalkStyle style)
        {
            if (!ui.Visible)
            {
                var backupData = new Dictionary<string, string>(){
                    {"speaker_name", name.TextValue },
                    {"player_name", pi.ClientState.LocalPlayer.Name},
                    {"player_gender", PlayerGender },
                    {"translation", ""},
                    {"original", text.TextValue}
                };

                saveToLocalhost(backupData);

                return;
            }
            PluginLog.Verbose(text.TextValue);
            translateTalk(name, text);
        }
        async void translateTalk(SeString name, SeString text)
        {
            var queryParameters = new Dictionary<string, string>(){
                {"target_lang", configuration.deepl_language},
                {"auth_key", configuration.deepl_key},
                {"text", text.TextValue}
            };
            try
            {

                var content = new FormUrlEncodedContent(queryParameters);
                HttpResponseMessage response = await httpClient.PostAsync(configuration.deepl_endpoint, content);
                var body = await response.Content.ReadAsStringAsync();
                TranslationResponse translationResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<TranslationResponse>(body);
                string translatedString = translationResponse.translations[0].text;
                applyTranslation(name.TextValue, translatedString);

                var backupData = new Dictionary<string, string>(){
                    {"speaker_name", name.TextValue },
                    {"player_name", pi.ClientState.LocalPlayer.Name},
                    {"player_gender", PlayerGender },
                    {"translation", translatedString},
                    {"original", text.TextValue}
                };

                saveToLocalhost(backupData);
            }
            catch (HttpRequestException e)
            {
                PluginLog.Error(e.ToString());
            }
        }

        async void saveToLocalhost(Dictionary<string, string> message)
        {
            if (!configuration.use_local_endpoint) return;
            try
            {
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(message);
                string mediaType = "application/json";
                var encoding = System.Text.Encoding.UTF8;
                var content = new StringContent(json, encoding, mediaType);
                var response = await httpClient.PostAsync(configuration.local_endpoint, content);
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                PluginLog.Error(e.ToString());
            }
        }
    }

    public class TranslationResponse
    {
        public List<DetectedTranslation> translations { get; set; }
    }

    public class DetectedTranslation
    {
        public string detected_source_language;
        public string text;
    }
}
