﻿﻿using ImGuiNET;
using System;
using System.Numerics;
using System.Text;
using System.Globalization;

namespace SimpleTranslationPlugin
{
    // It is good to have this be disposable in general, in case you ever need it
    // to do any cleanup
    class PluginUI : IDisposable
    {
        private Configuration configuration;

        // this extra bool exists for ImGui, since you can't ref a property
        private bool visible = false;
        private string text = "";
        private string author = "...";
        public bool Visible
        {
            get { return this.visible; }
            set { this.visible = value; }
        }
        private bool settingsVisible = false;
        public bool SettingsVisible
        {
            get { return this.settingsVisible; }
            set { this.settingsVisible = value; }
        }

        public PluginUI(Configuration configuration)
        {
            this.configuration = configuration;
        }

        public void Dispose()
        {
        }

        public void changeText(string text)
        {
            this.text = text;
        }

        public void changeAuthor(string text)
        {
            this.author = text;
        }

        public void Draw()
        {
            // This is our only draw handler attached to UIBuilder, so it needs to be
            // able to draw any windows we might have open.
            // Each method checks its own visibility/state to ensure it only draws when
            // it actually makes sense.
            // There are other ways to do this, but it is generally best to keep the number of
            // draw delegates as low as possible.

            DrawMainWindow();
            DrawSettingsWindow();
        }

        public void DrawMainWindow()
        {
            if (!Visible)
            {
                return;
            }

            if (settingsVisible)
            {
                return;
            }

            ImGui.SetNextWindowSize(new Vector2(450, 450), ImGuiCond.FirstUseEver);
            ImGui.SetNextWindowSizeConstraints(new Vector2(375, 330), new Vector2(float.MaxValue, float.MaxValue));
            if (ImGui.Begin("Simple Translation for XIV", ref this.visible, ImGuiWindowFlags.NoScrollbar | ImGuiWindowFlags.NoScrollWithMouse))
            {
                ImGui.Spacing();
                float windowScale = this.configuration.window_size;
                ImGui.SetWindowFontScale(windowScale);
                ImGui.TextWrapped($"{this.author}");
                ImGui.Spacing();
                ImGui.TextWrapped($"{this.text}");
                ImGui.Spacing();
            }
            ImGui.End();
        }

        public void DrawSettingsWindow()
        {
            if (!SettingsVisible)
            {
                return;
            }

            this.visible = false;

            ImGui.SetNextWindowSize(new Vector2(450, 250), ImGuiCond.Always);
            if (ImGui.Begin("Simple Translation for XIV | Settings", ref this.settingsVisible,
                ImGuiWindowFlags.NoResize | ImGuiWindowFlags.NoCollapse | ImGuiWindowFlags.NoScrollbar | ImGuiWindowFlags.NoScrollWithMouse))
            {
                var deeplKey = this.configuration.deepl_key;
                if (ImGui.InputText("DeepL API Key", ref deeplKey, 255))
                {
                    this.configuration.deepl_key = deeplKey;
                    this.configuration.Save();
                }
                if (this.configuration.use_local_endpoint)
                {
                    var localEndpoint = this.configuration.local_endpoint;
                    if (ImGui.InputText("Local Endpoint", ref localEndpoint, 255))
                    {
                       this.configuration.local_endpoint = localEndpoint;
                       this.configuration.Save(); 
                    }
                }
                var deeplEndpoint = this.configuration.deepl_endpoint;
                if (ImGui.InputText("DeepL API Endpoint", ref deeplEndpoint, 255))
                {
                    this.configuration.deepl_endpoint = deeplEndpoint;
                    this.configuration.Save();
                }
                var deeplLanguage = this.configuration.deepl_language;
                if (ImGui.InputText("DeepL Target Language", ref deeplLanguage, 2))
                {
                    this.configuration.deepl_language = deeplLanguage;
                    this.configuration.Save();
                }

                var windowSize = this.configuration.window_size;
                if (ImGui.InputFloat("Window Size", ref windowSize, 0.1f))
                {
                    this.configuration.window_size = windowSize;
                    this.configuration.Save();
                }

                var gender = this.configuration.gender;
                if (ImGui.InputText("Character gender", ref gender, 6))
                {
                    this.configuration.gender = gender;
                    this.configuration.Save();
                }
                var useLocalEndpoint = this.configuration.use_local_endpoint;
                if(ImGui.Checkbox("Use Local Endpoint", ref useLocalEndpoint))
                {
                    this.configuration.use_local_endpoint = useLocalEndpoint;
                    this.configuration.Save();
                }
                var removeAccents = this.configuration.remove_accents;
                if(ImGui.Checkbox("Remove Accents (Use if your translated text has \"?\" instead of accents", ref removeAccents))
                {
                    this.configuration.remove_accents = removeAccents;
                    this.configuration.Save();
                }
            }
            ImGui.End();
        }
    }
}